import * as Confluence from './confluence'
import * as Jira from './jira'

interface ConfluenceContainers {
  v1: Confluence.v1.Container
  v2: Confluence.v2.Container
}

interface JiraContainers {
  v2: Jira.v2.Container
}

interface Atlassian {
  confluence: ConfluenceContainers
  jira: JiraContainers
}

export class Container implements Atlassian {

  constructor (
    public readonly confluence: ConfluenceContainers,
    public readonly jira: JiraContainers
  ) { }

  public static async new (domain: string, username: string, password: string): Promise<Container> {
    const args = [domain, username, password] as const;
    return new Container({
      v1: await Confluence.v1.Container.new(...args),
      v2: await Confluence.v2.Container.new(...args),
    }, {
      v2: await Jira.v2.Container.new(...args),
    });
  }
  public static async fromEnvironment (): Promise<Container> {
    const get = (key: string): string => {
      if (process.env[key] !== undefined) {
        return process.env[key] as string;
      }
      throw new Error(`Missing environment variable ${key}`);
    }
    const domain = get('ATLASSIAN_DOMAIN');
    const username = get('ATLASSIAN_USERNAME');
    const password = get('ATLASSIAN_PASSWORD');
    return Container.new(domain, username, password);
  }
}
