import { expect, describe, it, beforeEach } from '@jest/globals';
import { Container, Confluence, Jira } from '../src';

beforeEach(() => {
  jest.resetModules();
})

describe('Container', () => {
  it('New', async () => {
    const container = await Container.new('http://localhost', 'username', 'password');
    expect(container.confluence.v1).toBeInstanceOf(Confluence.v1.Container);
    expect(container.confluence.v2).toBeInstanceOf(Confluence.v2.Container);
    expect(container.jira.v2).toBeInstanceOf(Jira.v2.Container);
  });
  it('fromEnvironment', async () => {
    expect(async () => await Container.fromEnvironment()).rejects.toThrow();
    process.env.ATLASSIAN_DOMAIN = 'http://localhost';
    expect(async () => await Container.fromEnvironment()).rejects.toThrow();
    process.env.ATLASSIAN_USERNAME = 'username';
    expect(async () => await Container.fromEnvironment()).rejects.toThrow();
    process.env.ATLASSIAN_PASSWORD = 'password';
    const container = await Container.fromEnvironment();
    expect(container.confluence.v1).toBeInstanceOf(Confluence.v1.Container);
    expect(container.confluence.v2).toBeInstanceOf(Confluence.v2.Container);
    expect(container.jira.v2).toBeInstanceOf(Jira.v2.Container);
  });
});
